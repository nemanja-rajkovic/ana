import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class KorisnikService {
  private readonly API_URL = environment.baseUrl + '/api/users';
  dataChange: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

  constructor(private _http: HttpClient) { }

  public getAllKorisnici(): Observable<User[]> {
    this._http.get<User[]>(this.API_URL).subscribe(data => {
      this.dataChange.next(data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
    return this.dataChange.asObservable();
  }

}
